CONTAINER ?= "poc-kafka-connect_db_1"
KAFKA_DIR ?= "kafka_2.13-2.6.3"

VIRTUALENV_DIR ?= venv
DOCKER_COMPOSE ?= ./$(VIRTUALENV_DIR)/bin/docker-compose
PYTHON ?= ./$(VIRTUALENV_DIR)/bin/python3

BENCH_SCALE ?= 20
BENCH_CLIENTS ?= 10
BENCH_JOBS ?= 5
BENCH_TIME ?= 10
BENCH_MAX_TPS ?= 10

virtualenv:
	virtualenv ./$(VIRTUALENV_DIR)
	$(PYTHON) -m pip install pip-tools

pip-compile:
	./$(VIRTUALENV_DIR)/bin/pip-compile -o requirements.txt requirements.in

pip-sync:
	./$(VIRTUALENV_DIR)/bin/pip-sync requirements.txt

psql:
	$(DOCKER_COMPOSE) exec -u postgres db psql template1

services-up:
	$(DOCKER_COMPOSE) up --remove-orphans db zk-1 kafka-1

#
# pgbench
#  - remembner: BENCH_SCALE >= BENCH_CLIENTS
#

#pgbench-db-setup:
#	docker exec -ti $(CONTAINER) su - postgres -c 'psql -q -d bench -c '\d' > /dev/null 2> /dev/null || createdb bench || true'
#	docker exec -ti $(CONTAINER) su - postgres -c 'pgbench -i --scale=$(BENCH_SCALE) --foreign-keys bench'
#	$(MAKE) pgbench-db-dump

#pgbench-db-dump-table:
#	@echo ""
#	@echo "# ===================================================================================================="
#	@echo "# $(TABLE)"
#	@echo "# ===================================================================================================="
#	@echo ""
#	@docker exec -ti $(CONTAINER) su - postgres -c 'psql -d bench -c "\d $(TABLE)"'
#	@docker exec -ti $(CONTAINER) su - postgres -c 'psql -d bench -c "select * from $(TABLE) order by bid limit 5"'

#pgbench-db-count-table:
#	@echo -n COUNT $(TABLE) ; docker exec -ti $(CONTAINER) su - postgres -c 'psql -d bench -t -c "select count(*) from $(TABLE)"'

#db-count:
#	@$(MAKE) -s TABLE="pgbench_branches" pgbench-db-count-table
#	@$(MAKE) -s TABLE="pgbench_tellers" pgbench-db-count-table
#	@$(MAKE) -s TABLE="pgbench_accounts" pgbench-db-count-table
#	@$(MAKE) -s TABLE="pgbench_history" pgbench-db-count-table

#pgbench-db-dump:
#	@$(MAKE) -s TABLE="pgbench_branches" pgbench-db-dump-table
#	@$(MAKE) -s TABLE="pgbench_tellers" pgbench-db-dump-table
#	@$(MAKE) -s TABLE="pgbench_accounts" pgbench-db-dump-table
#	@$(MAKE) -s TABLE="pgbench_history" pgbench-db-dump-table
#	@echo
#	@$(MAKE) -s db-count

#pgbench-run:
#	docker exec -ti $(CONTAINER) su - postgres -c \
#		'pgbench -c $(BENCH_CLIENTS) -j $(BENCH_JOBS) --time=$(BENCH_TIME) --progress=1 --no-vacuum --rate=$(BENCH_MAX_TPS) bench'

#
# clic simulator
#

CLIC_DB ?= 'clic'

clic-create-db:
	docker exec -ti $(CONTAINER) su - postgres -c 'psql -q -d $(CLIC_DB) -c '\d' > /dev/null 2> /dev/null || createdb $(CLIC_DB) || true'

clic-recreate-table:
	env PGHOST=localhost PGPORT=15432 PGDATABASE=$(CLIC_DB) PGUSER=postgres PGPASSWORD=postgres \
		$(PYTHON) -m pybench --jobs 4 pybench.simulators.clic.Simulator table_drop_if_exists table_create

clic-recreate-table-and-insert:
	env PGHOST=localhost PGPORT=15432 PGDATABASE=$(CLIC_DB) PGUSER=postgres PGPASSWORD=postgres \
		$(PYTHON) -m pybench --jobs 4 pybench.simulators.clic.Simulator table_drop_if_exists table_create insert:repeat

clic-create-table-and-insert-repeat:
	env PGHOST=localhost PGPORT=15432 PGDATABASE=$(CLIC_DB) PGUSER=postgres PGPASSWORD=postgres \
		$(PYTHON) -m pybench --jobs 4 pybench.simulators.clic.Simulator table_create insert:repeat

clic-create-table-and-insert-timelimit:
	env PGHOST=localhost PGPORT=15432 PGDATABASE=$(CLIC_DB) PGUSER=postgres PGPASSWORD=postgres \
		$(PYTHON) -m pybench --jobs 4 pybench.simulators.clic.Simulator table_create insert:timelimit

clic-dump-db:
	docker exec -ti $(CONTAINER) su - postgres -c 'psql -d $(CLIC_DB) -c "select * from pybench_clic"'

clic-kafka-connect-standalone:
	$(DOCKER_COMPOSE) up --remove-orphans --build kafka-connect-standalone-1

clic-kafka-dump-topic:
	$(KAFKA_DIR)/bin/kafka-console-consumer.sh --bootstrap-server localhost:19092 \
		--from-beginning --topic clic_pybench_clic

#
# kafka
#

kafka-setup-topics:
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --if-not-exists --create --topic sample-topic --partitions 3 --replication-factor 1

kafka-list-topics:
	$(KAFKA_DIR)/bin/kafka-topics.sh --bootstrap-server localhost:19092 --describe

#kafka-connect-create-topics:
#	$(KAFKA_DIR)/bin/kafka-topics.sh --create --bootstrap-server localhost:19092 \
#		--topic connect-configs --replication-factor 1 --partitions 1 --config cleanup.policy=compact
#	$(KAFKA_DIR)/bin/kafka-topics.sh --create --bootstrap-server localhost:19092 \
#		--topic connect-offsets --replication-factor 1 --partitions 50 --config cleanup.policy=compact
#	$(KAFKA_DIR)/bin/kafka-topics.sh --create --bootstrap-server localhost:19092 \
#		--topic connect-status --replication-factor 1 --partitions 10 --config cleanup.policy=compact
