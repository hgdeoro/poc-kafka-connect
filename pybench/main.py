import argparse
import importlib
import logging
import logging.config
import multiprocessing
import signal
from argparse import Namespace
from multiprocessing import Process

import psycopg2
from psycopg2 import extensions
from psycopg2.extensions import connection as Connection

from pybench import simulator, method, logconfig
from pybench.method import Method

logger = logging.getLogger(__name__)


class Main:

    def __init__(self):
        self.parser: argparse.ArgumentParser = None
        self.args: Namespace = None

    def setup_argparse(self):
        assert self.parser is None
        assert self.args is None
        self.parser: argparse.ArgumentParser = argparse.ArgumentParser()
        self.parser.add_argument('fqcn', type=str)
        self.parser.add_argument('methods', metavar='N', type=str, nargs='+')
        self.parser.add_argument('--jobs', type=int, default=2)

    def parse_args(self):
        assert self.args is None
        self.args: Namespace = self.parser.parse_args()

    def setup_logging(self):
        assert self.args is not None
        logging.config.dictConfig(logconfig.CONFIG)

    @staticmethod
    def get_simulator_class(args_fqcn: str):
        module_name, class_name = args_fqcn.rsplit('.', maxsplit=1)
        logger.debug("module_name=%s class_name=%s", module_name, class_name)

        module_obj = importlib.import_module(module_name)
        logger.debug("module_obj=%s", module_obj)

        class_obj = getattr(module_obj, class_name)
        logger.debug("class_obj=%s", class_obj)

        return class_obj

    @classmethod
    def get_method_descriptors(cls, args_methods: list[str]) -> list[Method]:
        return [
            Method.parse(method_descriptor)
            for method_descriptor in args_methods
        ]

    @classmethod
    def _inner_run(cls, job_id, queue: multiprocessing.Queue, args_fqcn, method_desc: method.Method):
        simulator_class = cls.get_simulator_class(args_fqcn)
        connection: Connection = psycopg2.connect('')
        with open(f"/tmp/job-{job_id}.csv", 'w') as output_trace_file:
            simulator_obj: simulator.BaseSimulator = simulator_class(output_trace_file)
            method_obj = method_desc.get_method(simulator_obj)
            strategy = method_desc.get_strategy()
            strategy.call(method_obj, connection, queue)

    def run(self):
        assert self.args is not None

        method_descriptors = self.get_method_descriptors(self.args.methods)
        for method_desc in method_descriptors:
            logger.info("Running method %s", method_desc)
            if method_desc.get_strategy().PARALLEL:
                jobs = self.args.jobs
            else:
                jobs = 1

            args_list_of_lists = [
                (job_id, multiprocessing.Queue(), self.args.fqcn, method_desc)
                for job_id in range(jobs)
            ]
            processes = [
                Process(
                    target=self._inner_run,
                    args=args,
                    daemon=True,
                    name=f"job-{args[0]}",
                )
                for args in args_list_of_lists
            ]

            def sigint_handler(signum, frame):
                for args in args_list_of_lists:
                    queue: multiprocessing.Queue = args[1]
                    queue.put('STOP')

            signal.signal(signal.SIGINT, sigint_handler)

            _ = [
                p.start() for p in processes
            ]
            _ = [
                p.join() for p in processes
            ]
            assert all([p.is_alive() is False for p in processes])
            for p in processes:
                if p.exitcode != 0:
                    logger.error("Exit status != 0. exitcode=%s", p.exitcode)

            assert sum([p.exitcode for p in processes]) == 0


def run():
    main = Main()
    main.setup_argparse()
    main.parse_args()
    main.setup_logging()
    main.run()
