import csv


class BaseSimulator:
    FIELDNAMES: list[str] = []

    def __init__(self, output_file):
        assert self.FIELDNAMES
        self._csv_writer = csv.DictWriter(output_file, fieldnames=self.FIELDNAMES)

    def write_record(self, **kwargs: dict[str, str]):
        self._csv_writer.writerow(kwargs)
