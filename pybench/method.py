import dataclasses
import typing

from pybench import call_strategy


@dataclasses.dataclass
class Method:
    descriptor: str
    method_name: str
    strategy_name: str

    def get_method(self, simulator_obj) -> typing.Callable:
        return getattr(simulator_obj, self.method_name)

    def get_strategy(self) -> call_strategy.CallStrategy:
        if self.strategy_name is None:
            return call_strategy.Once()
        elif self.strategy_name.lower() == 'once':
            return call_strategy.Once()
        elif self.strategy_name.lower() == 'repeat':
            return call_strategy.Repeat()
        elif self.strategy_name.lower() == 'timelimit':
            return call_strategy.TimeLimit()
        else:
            raise Exception(f"Invalid strategy='{self.strategy_name}' descriptor='{self.descriptor}'")

    @staticmethod
    def parse(descriptor: str):
        parts = descriptor.split(':')
        method_name = parts[0]
        strategy_name = parts[1] if len(parts) > 1 else None
        return Method(
            descriptor=descriptor,
            method_name=method_name,
            strategy_name=strategy_name,
        )
