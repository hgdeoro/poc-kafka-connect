import logging

from faker import Faker
from faker.providers import internet
from psycopg2.extensions import cursor as Cursor

from pybench import simulator

logger = logging.getLogger(__name__)


class Simulator(simulator.BaseSimulator):
    FIELDNAMES = ['id', 'url', 'source_ip']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake = Faker()
        self.fake.add_provider(internet)

    def table_drop_if_exists(self, cur: Cursor = None, **kwargs):
        cur.execute("""
        DROP TABLE IF EXISTS pybench_clic;
        """)

    def table_create(self, cur: Cursor = None, **kwargs):
        cur.execute("""
        CREATE TABLE IF NOT EXISTS pybench_clic (
            id serial not null primary key,
            url text not null,
            source_ip text not null
        ); 
        """)

    def table_truncate(self, cur: Cursor = None, **kwargs):
        cur.execute("""
        TRUNCATE TABLE pybench_clic; 
        """)

    def insert(self, cur: Cursor = None, **kwargs):
        uri = self.fake.uri()
        source_ip = self.fake.ipv4()
        logger.debug("uri=%s source_ip=%s", uri, source_ip)
        cur.execute("""
        INSERT INTO pybench_clic
        (url, source_ip)
        VALUES
        (%s, %s)
        RETURNING id
        """, (uri, source_ip))

        clic_id = cur.fetchone()[0]
        self.write_record(id=clic_id, url=uri, source_ip=source_ip)
