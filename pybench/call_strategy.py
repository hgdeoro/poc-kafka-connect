import abc
import inspect
import logging
import multiprocessing
import os
import time
from queue import Empty as QueueEmpty

from psycopg2.extensions import connection as Connection
from psycopg2.extensions import cursor as Cursor

logger = logging.getLogger(__name__)


class CallStrategy(abc.ABC):
    PARALLEL = False

    def __init__(self):
        self._keep_running = True

    @abc.abstractmethod
    def call(self, method, conn: Connection, queue: multiprocessing.Queue):
        raise NotImplementedError()

    def _check_queue(self, queue: multiprocessing.Queue):
        try:
            msg: str = queue.get(block=False)
            if msg.lower() == 'stop':
                self._keep_running = False
                logger.debug("Setting self._keep_running = False")
            else:
                logger.warning(f"Invalid message read from queue: '{msg}'")
        except QueueEmpty:
            pass


class Once(CallStrategy):
    def call(self, method, conn: Connection, queue: multiprocessing.Queue):
        with conn:
            with conn.cursor() as cursor:
                method(conn=conn, cur=cursor)


class Repeat(CallStrategy):
    PARALLEL = True
    DEFAULT_REPEAT = 10

    @property
    def repeat(self):
        return int(os.environ.get('REPEAT', self.DEFAULT_REPEAT))

    def call(self, method, conn: Connection, queue: multiprocessing.Queue):
        for _ in range(self.repeat):
            with conn:
                with conn.cursor() as cursor:
                    method(conn=conn, cur=cursor)
            self._check_queue(queue)
            if not self._keep_running:
                break


class TimeLimit(CallStrategy):
    PARALLEL = True
    DEFAULT_TIME_LIMIT = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._time_limit: float = None

    @property
    def time_limit(self) -> float:
        if self._time_limit is None:
            self._time_limit = float(os.environ.get('TIME_LIMIT', self.DEFAULT_TIME_LIMIT))
        return self._time_limit

    def call(self, method, conn: Connection, queue: multiprocessing.Queue):
        start_time = time.monotonic()
        while self._keep_running and time.monotonic() < (start_time + self.time_limit):
            with conn:
                with conn.cursor() as cursor:
                    cursor: Cursor
                    method(conn=conn, cur=cursor)
            self._check_queue(queue)
