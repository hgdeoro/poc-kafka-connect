import logging

logger = logging.getLogger(__name__)


if __name__ == '__main__':
    from pybench import main
    main.run()
